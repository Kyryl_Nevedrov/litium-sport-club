//
//  GaleryCollectionViewController.swift
//  litium
//
//  Created by Kyryl Nevedrov on 10/11/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit

private let reuseIdentifier = "galeryCell"

class GaleryCollectionViewController: UICollectionViewController {
    fileprivate let sectionInsets = UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
    fileprivate let itemsPerRow: CGFloat = 3.0
    
    private let sportSections = SportSection.getSportSections()
    private var sportSectionsWithGallery: [SportSection] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        sportSectionsWithGallery = sportSections.filter { sportSection in
            return sportSection.galleryImages != nil
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailGallery" {
            let destinationController = segue.destination as! DetailGaleryViewController
            let indexPath = collectionView?.indexPathsForSelectedItems
            destinationController.image = sportSectionsWithGallery[indexPath![0].section].galleryImages![indexPath![0].row]
        }
    }

    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sportSectionsWithGallery.count
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sportSectionsWithGallery[section].galleryImages!.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! GalleryCollectionViewCell
        cell.imageView.image = sportSectionsWithGallery[indexPath.section].galleryImages![indexPath.row]
    
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "galleryHeader", for: indexPath) as! GalleryHeaderCollectionReusableView
            headerView.label.text = sportSectionsWithGallery[indexPath.section].title
            return headerView
        default:
            assert(false, "Unexpected element kind")
        }
    }
}

extension GaleryCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddings = sectionInsets.left * (itemsPerRow + 1)
        let availabeWidth = view.frame.width - paddings
        let itemWidth = availabeWidth / itemsPerRow
        
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
