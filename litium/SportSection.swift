//
//  SportSection.swift
//  litium
//
//  Created by Kyryl Nevedrov on 10/11/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class SportSection{
    var title: String
    var description: String
    var schedule: String
    var prices: String
    var image: UIImage?
    var trainers: [Trainer]?
    var galleryImages: [UIImage]?
    
    init(title: String, description: String, schedule: String, prices: String, image: UIImage?, trainers: [Trainer]?, galleryImages: [UIImage]?){
        self.title = title
        self.description = description
        self.schedule = schedule
        self.prices = prices
        self.image = image
        self.trainers = trainers
        self.galleryImages = galleryImages
    }
    
    static func getSportSections() -> [SportSection] {
        return [
            SportSection(title: "Тренажёрный зал",
                         description: "\tТренажерный зал – место, без которого не мыслим фитнес клуб, место в котором, собственно, и идет основная работа над телом. Традиционно занятия в тренажерном зале считаются самым эффективным средством коррекции фигуры. Здесь вы можете не только набрать мышечную или снизить жировую массу тела, но и повысить выносливость, укрепить сердечно-сосудистую и дыхательную системы.\n\tТренажёрный зал спортивного клуба \"Litium\" предоставляет своим клиентам лучшие условия для тренировок под руководством опытных тренеров. Клуб оснащен современным оборудованием лучших фирм которое соответствует современным требованиям безопасности.\n- Тренера, являющиеся неоднократные чемпионами по пауэрлифтингу и бодибилдингу.\n- Просторный зал площадью 500 кв.м.\n- Современные тренажеры INTER ATLETIKA и JONSON\n- Зона кардио тренажеров\n- Зал бокса\n- Мощный климат контроль.\n-  Фреш-бар для поддержания энергетического запаса организма.",
                         schedule: "- Пн-Пт: с 08:30 до 22:00\n- Сб: с 08:30 до 20:00\n- Воскресенье: с 12:00 до 18:00\n- По детскому абонементу занятия разрешены только до 18:00 во все дни.\n- Продолжительность занятий в зале по времени не ограничена.\n- Занятия в тренажёрном зале заканчиваются за 15 минут до закрытия клуба!",
                         prices: "Разовая тренировка = 50 грн.\n- Абонемент месячный на 2-х разовое посещение в неделю (9 раз в мес.) = 290 грн.\n- Абонемент месячный на 3-х разовое посещение в неделю (13 раз в мес.) = 340 грн.\n- Абонемент месячный на 4-х разовое посещение в неделю (17 раз в мес.) = 400 грн.\n- Абонемент месячный на ежедневное посещение = 450 грн.\n- Детская разовая тренировка = 25 грн. (от 12 до 16 лет)\n- Детский абонемент месячный на 2-х разовое посещение в неделю = 180 грн. (от 12 до 16 лет)\n- Детский абонемент месячный на 3-х разовое посещение в неделю = 220 грн. (от 12 до 16 лет)\n- Детский абонемент месячный на 4-х разовое посещение в неделю = 260 грн. (от 12 до 16 лет)",
                         image: UIImage(named: "trenazher"),
                         trainers: getTrainersForGym(), galleryImages: getGalleryImagesForGym()),
            SportSection(title: "Аэробика",
                         description: "\tКомплекс упражнений включает в себя ходьбу, бег, прыжки, упражнения на гибкость. Результат регулярных занятий аэробикой — поддержание тела в тонусе, тренировка мышц и кожи, общее оздоровление организма. Используется в профилактических и лечебно-оздоровительных целях.\n\tАЭРОБНЫЕ КЛАССЫ - это занятия на развитие выносливости сердечно-сосудистой системы, улучшение функциональных возможностей организма и уменьшению жировой массы тела. К ним относятся:\n\tSTEP BEGINNERS Оздоровительная аэробика с использованием специальной степ-платформы с регулируемой высотой. Основная часть занятия состоит из базовых шагов аэробики, что не исключает и танцевальной стилизации упражнений. Движения, выполняемые ногами, не сложны, естественны, как при ходьбе по лестнице. Для изменения интенсивности занятия достаточно изменить высоту платформы. Таким образом, в одной группе одновременно могут заниматься люди с разным уровнем подготовки, и физическая нагрузка для каждого будет индивидуальна.\n\tSTEP & POWER Интервальная тренировка. Чередование аэробной нагрузки на степ-платформе с силовым комплексом упражнений. В аэробной части занятия выполняется несложный блок базовой степ-аэробики . Все силовые упражнения можно усложнить (облегчить) или просто разнообразить, используя различную высоту платформы и дополнительное оборудование (гантели, бодибары , резиновые амортизаторы). В силовой части занятия выполняются упражнения на развитие силы всех мышечных групп.",
                         schedule: "- Пн, ср, пт - 18:00\n- Вт, чт - 19:00",
                         prices: "Разовая тренировка – 40 грн.\n- Абонемент месячный на 2-х разовое посещение в неделю (9 занятий) – 250 грн.\n- Абонемент месячный на 3-х разовое посещение в неделю (13 занятий) – 300 грн.",
                         image: UIImage(named: "aerobika"),
                         trainers: getTrainersForAerobics(), galleryImages: getGalleryImagesForAerobics()),
            SportSection(title: "Шейпинг",
                         description: "\tШейпинг это методика целенаправленного оздоровления организма и изменение фигуры организма женщины, которая состоит из специальной программы физических упражнений и соответствующей системы питания.\n\tШейпинг позволяет реализовать все направления пластического изменения тела, для женщин любых возрастов, и любой спортивной подготовкой. Он способствует увеличению или уменьшению объема мышечной ткани, снижению содержания жиров в организме и т.д. Занятия шейпингом повсеместно и последовательно нагружают все группы мышц. Изменяя нагрузки на конкретные области тела и контролируя обменными процессами, путем организации правильного питания и отдыха, вы в силах решить различные задачи формирования телосложения.",
                         schedule: "- Пн, ср, пт, сб - 10:00\n- Вт, чт - 18:00\n- Пн, ср, пт - 19:00\n- Пн, ср, пт - 20:00",
                         prices: "Разовая тренировка – 40 грн.\n- Абонемент месячный на 2-х разовое посещение в неделю (9 занятий) – 250 грн.\n- Абонемент месячный на 3-х разовое посещение в неделю (13 занятий) – 300 грн.",
                         image: UIImage(named: "sheiping"),
                         trainers: getTrainersForShaping(), galleryImages: getGalleryImagesForShaping()),
            SportSection(title: "Пилатес",
                         description: "\tКомплексные силовые упражнения, выполняемые при постоянном контроле положения своего тела в пространстве и специальном способе дыхания.\n\tЦель занятий:\n- улучшение гибкости и подвижности в суставах\n- улучшение эластичности мышц\n- улучшение состояния позвоночника.\n\tПозволяет тонизировать и удлинить мышцы, не накапливая мышечной массы.\n\t Одной из главных задач данной программы является формирование правильной осанки. А приятный результат систематических занятий – плоский живот. В процессе тренировки используются упражнения преимущественно статического характера, асаны из йоги, специфические дыхательные упражнения. Использование различного оборудования ( фитболов ), позволяет сделать данный комплекс разнообразным.",
                         schedule: "- Пн, пт - 17:00\n- Вт, чт - 19:00",
                         prices: "Разовая тренировка – 40 грн.\n- Абонемент месячный на 2-х разовое посещение в неделю (9 занятий) – 260 грн.\n- Абонемент месячный на 3-х разовое посещение в неделю (13 занятий) – 310 грн.",
                         image: UIImage(named: "pilates"),
                         trainers: getTrainersForPilates(), galleryImages: getGalleryImagesForPilates()),
            SportSection(title: "Йога",
                         description: "\tЙога - означает \"единение\", в популярном понимании, эта система методов и практик работы с телом и сознанием. Повышает силу, гибкость, наполняет спокойствием и комфортом, учит общению со своим телом.\n\tЙога  — это уникальная система упражнений, которая помогает организму не стареть, бороться с болезнями и одновременно успокаивать нервы, снимать стресс. Йога подходит мужчинам и женщинам, молодым и пожилым, на практике убеждает в своей целительной силе.\n\tЗаниматься йогой — значит привести в гармонию своё сознание и существование, понять смысл жизни, ощутить радость бытия и стать по-настоящему свободным от болезней, стрессов, негативного влияние окружения, от метафизических страхов. Ваша осанка станет идеальной. Ваше тело и душа обретут гармонию. В результате выполнения всего комплекса упражнений Ваши мышцы станут — сильнее, а тело приобретёт дополнительную гибкость. ",
                         schedule: "- Пн, ср, пт - 09:00\n- Пн, ср, пт - 18:00\n- Пн, ср, пт - 20:10",
                         prices: "Разовая тренировка = 40 грн.\n- Абонемент на 3 занятия подряд = 100 грн.\n- Абонемент месячный на 2-х разовое посещение (9 занятий) – 300 грн.\n- Абонемент месячный на 3-х разовое посещение (13 занятий) – 400 грн.",
                         image: UIImage(named: "joga"),
                         trainers: getTrainersForYoga(), galleryImages: getGalleryImagesForYoga()),
            SportSection(title: "Кикбоксинг",
                         description: "\tКикбоксинг - вид спорта на основе восточных единоборств: каратэ, тхэквондо, ушу, английский бокс. Эти занятия хорошо укрепляют тело, формируют дух, развивают уверенность в себе, воспитывают выдержку. Детский кикбоксинг проходит в игровой форме.\n\tНаправления:\n\tЛайт-контакт (Лёгкий контакт) - Раздел кикбоксинга, в котором запрещены сильные, акцентированные удары руками и ногами. Из-за этих ограничений темп боя выше, чем в фулл-контакте. Победа присуждается спортсмену, продемонстрировавшему лучшую технику работы рук и ног и, соответственно, нанесшему большее количество ударов ногами и руками.\n\tСеми-контакт (Ограниченный контакт) - Раздел кикбоксинга, для которого характерно запрещение сильных ударов.(за исключением, когда соперник неожиданно пошел на удар). при этом засчитывается нокаут или нокдаун (если он есть), но очки при этом не засчитываются. Поединок носит прерывистый характер, так как после проведения чистого удара рукой или ногой судья останавливает бой и начисляет очки. Предпочтение отдается ударам ногами, и выше всего из них, оцениваются прыжковые удары.\n\tФулл-контакт (Полный контакт) - Раздел кикбоксинга, в котором удары ногами и руками наносятся без ограничений силы, в полный контакт. Также, как и в боксе, отсчитываются нокдауны и нокауты.\n\tФулл-контакт с лоу-кик - Раздел кикбоксинга, в котором разрешается наносить удары ногами по внешней и внутренней стороне бедра.",
                         schedule: "- Пн, ср, пт - 17:00 (группа совместная для парней и девушек)",
                         prices: "Разовая тренировка = 40 грн.\n- Абонемент месячный на 2-х разовое посещение (9 занятий) – 250 грн.\n- Абонемент месячный на 3-х разовое посещение (13 занятий) – 300 грн.",
                         image: UIImage(named: "kikboxing"),
                         trainers: getTrainersForKickboxing(), galleryImages: getGalleryImagesForKickboxing()),
            SportSection(title: "Капоэйра",
                         description: "\tКапоэйра - афро-бразильский вид боевого искусства. Оно объединяет музыку, движения, борьбу, некоторые аспекты танца.\n\tБразильский вид боевого искусства, в котором используется преимущественно техника ног при тщательно разработанных перемещениях, включающих элементы акробатик.\n\tНа сегодняшний день капоэйра является культовой системой единоборств с сотнями тысяч приверженцев во всем мире.Если вы еще сомневаетесь стоит ли посетить наши занятия. Из капоэйра вышли: современный хип-хоп и брейк-данс, зажигательная латиноамериканская музыка... и не забывайте - капоэйра - это грозное боевое исскуство, которое может защитить тебя в любой жизненной ситуации.\n\tКапоэйра - это прежде всего игра умов, а не мускулов. Выигрывает тот кто лучше думает.",
                         schedule: "- Вт, чт - 11:00 (детская группа)\n- Вт, чт - 18:00 (детская группа)\n- Вт, чт - 19:00 (детская группа)\n- Пн, ср, пт - 20:00 (взрослая группа)",
                         prices: "Разовая тренировка = 40 грн.\n- Абонемент месячный на 2-х разовое посещение (9 занятий) – 250 грн.\n- Абонемент месячный на 3-х разовое посещение (13 занятий) – 300 грн.",
                         image: UIImage(named: "capoeiro"),
                         trainers: getTrainersForCapoeira(), galleryImages: getGalleryImagesForCapoeira()),
            SportSection(title: "Карате",
                         description: "\tКарате - боевое искусство, система защиты и нападения без оружия, в котором применяются удары и блоки руками и ногами. Термин «каратэ» («китайская рука») был введён в обращение в ХVIII в. неким Сакугавой из окинавского местечка Аката. К началу ХХ века каратэ уже входит в обязательную программу подготовки личного состава японской армии. На начальном этапе каратэ представляло собой систему рукопашного боя, предназначавшуюся исключительно для самообороны. Сегодня каратэ приобрело куда большую известность благодаря показательным выступлениям, демонстрирующим силу удара каратиста.",
                         schedule: "Вт, чт - 17:00\n- Сб - 10:00",
                         prices: "Разовая тренировка = 40 грн.\n- Абонемент месячный на 2-х разовое посещение (9 занятий) – 250 грн.\n- Абонемент месячный на 3-х разовое посещение (13 занятий) – 300 грн.",
                         image: UIImage(named: "karate"),
                         trainers: getTrainersForKarate(), galleryImages: getGalleryImagesForKarate()),
            SportSection(title: "Стрип Денс",
                         description: "\tStrip-dance - вобрал в себя движения из современной хореографии, джаза, модерна, элементы классического балета, восточного танца, стречинга...\n\tТанец развивает гибкость, задействует мышцы рук, ног, груди, ягодиц, живота. Поможет лучше чувствовать свое тело и использовать его на все сто. Это исскуство сделает Ваши формы соблазнительными.\n\tНа занятиях Вас научат двигаться под музыку красиво и пластично. Вы кординально измените отношение к своему телу.\n\tСексуальность – это то что привлекает, составляющая нашей жизни, это то, что волнует, влечёт и манит.\n\tБез сексуальности невозможно представить полноценную жизнь и каждый находит свои пути выражения сексуальности.\n\tИменно поэтому стемительными темпами заваевывает популярность Strip Dance - способ выразить свои лучшие черты в танце.",
                         schedule: "Пн, ср, пт - 19:00",
                         prices: "Разовая тренировка – 40 грн.\n- Абонемент месячный на 2-х разовое посещение (9 занятий) – 250 грн.\n-Абонемент месячный на 3-х разовое посещение (13 занятий) – 300 грн.",
                         image: UIImage(named: "strip"),
                         trainers: getTrainersForStripDance(), galleryImages: getGalleryImagesForStripDance()),
            SportSection(title: "Массаж",
                         description: "\tНевозможно отдать предпочтение тем или иным народам в авторстве изобретения массажа. Он зарождался и развивался параллельно в разных странах и континентах.\n\tВ своём изначальном виде, то есть как простое трение, поглаживание, возник как лечебное средство на заре развития человечества. Судя по преданиям, люди, стремясь облегчить боли, производили растирание и поколачивание места травмы.\n\tВ настоящее время массаж употребляется как лечебное средство при многих болезнях.",
                         schedule: "Пн-пт 10:00-20:00",
                         prices: "Массаж вдоль позвоночника: 80 грн\n-Массаж общий: 120-150 грн (зависит от используемых ингридиентов)\n- Массаж антицелюлитный: от 150 грн\n- Аромомассаж базовий: 150 грн\n- Аромомассаж релакс: 200-250 грн",
                         image: UIImage(named: "massage"),
                         trainers: nil, galleryImages: getGalleryImagesForMassage()),
            SportSection(title: "Cолярий",
                         description: "\tВ последнее время улицы города и не только нашего заполонили смуглые и не очень девушки, в моде плотно укоренился культ загорелого тела.\n\tИскусственный загар увеличивает настроение и самооценку и в конце-концов это просто очень приятная процедура – ощущать, как искусственное солнце гладит своими нежными лучами кожу, в такие моменты не хочется думать о плохом и жизнь сразу видится в розовом цвете.\n\tЗагорайте и помните, что солярий не просто красивый загар и аппетитный вид кожи, это ещё и здоровье, хорошее настроение, а также повышение иммунитета. ",
                         schedule: "Пн-пт 10:00-20:00",
                         prices: "1 минута = 4 грн.",
                         image: UIImage(named: "solariy"),
                         trainers:  nil, galleryImages: getGalleryImagesForSolarium()),
            SportSection(title: "Косметология",
                         description: "\tПрессотерапия: Ультрасовременное устройство \"BTL-600 Lymphastim\" позволяет проведение терапии, направленной на лечение лимфатических отёков и улучшение кровоснабжения лимфатической системы.\n\tЛимфодренаж активизирует  поток лимфы в лимфатической системе, а она играет решающую роль в иммунной системе организма. В лимфатическом дренаже используются анатомические и физиологические знания системы циркуляции человека. Процедура стимулирует активность лимфатических токов, обновляет прохождение лимфы в критических местах и улучшает процесс обмена жидкости в теле.\n\tПрименение прессотерапии благотворно влияет на оздоровительные процессы в организме человека и является неотъемлемой частью современных программ по реабилитации и восстановлению.",
                         schedule: "Пн-пт 10:00-20:00",
                         prices: "По договоренности",
                         image: UIImage(named: "kosmet"),
                         trainers: nil, galleryImages: getGalleryImagesForCosmetology())
        ]
    }
    
    private static func getTrainersForGym() -> [Trainer]? {
        return [
            Trainer(name: "Загурский Валерий",
                    achievments: "Чемпион Хмельницкой области по бодибилдингу\nНеоднократный призёр Украины по бодибилдингу\n- 2-х кратный Чемпион Украины по бодибилдингу\nАбсолютный Чемпион Украины по бодибилдингу\n- Победитель кубка Украины по бодибилдингу\nМастер спорта по бодибилдингу",
                    imageName: "tr_gym_zagurskiy"),
            Trainer(name: "Басистый Андрей",
                    achievments: "Чемпион Хмельницкой области по пауэрлифтингу\n- Неоднократный призёр Украины по пауэрлифтингу\n- 4-х кратный Чемпион Украины по пауэрлифтингу\n- Мастер спорта по пауэрлифтингу",
                    imageName: "tr_gym_basistyy"),
            Trainer(name: "Вербицкий Владимир",
                    achievments: "Неоднократный призёр Украины по тяжёлой атлетике\n- Чемпион Украины по тяжёлой атлетике\n- Мастер спорта по тяжёлой атлетике",
                    imageName: "tr_gym_verbitskiy"),
            Trainer(name: "Василий Демьяненко",
                    achievments: "Мастер спорта Украины международного класса\n- Серебряный призер чемпионата мира по пауэрлифтингу",
                    imageName: "tr_gym_demyanenko"),
            Trainer(name: "Соловей Александр",
                    achievments: "",
                    imageName: "tr_gym_solovey")
        ]
    }
    
    private static func getTrainersForAerobics() -> [Trainer]? {
        return [
            Trainer(name: "Оксана Журба", achievments: "", imageName: "tr_aerobika_zhurba_oksana"),
            Trainer(name: "Юлия Швец", achievments: "", imageName: "tr_aerobike_yuliya_svets")
        ]
    }
    
    private static func getTrainersForShaping() -> [Trainer]? {
        return [
            Trainer(name: "Татьяна Ищенко", achievments: "", imageName: "tr_sheiping_tatiana_ischenko")
        ]
    }
    
    private static func getTrainersForPilates() -> [Trainer]? {
        return [
            Trainer(name: "Ищенко Татьяна", achievments: "", imageName: "tr_pilates_tatiana_ischenko")
        ]
    }
    
    private static func getTrainersForYoga() -> [Trainer]? {
        return [
            Trainer(name: "Вячеслав Морозов", achievments: "", imageName: "tr_yoga_vyacheslav_morozov")
        ]
    }
    
    private static func getTrainersForKickboxing() -> [Trainer]? {
        return [
            Trainer(name: "Максим Чайковский", achievments: "", imageName: "tr_kick_maksim_chaykovskiy")
        ]
    }
    
    private static func getTrainersForCapoeira() -> [Trainer]? {
        return [
            Trainer(name: "Грунин Павел", achievments: "", imageName: "tr_kapueyro")
        ]
    }
    
    private static func getTrainersForKarate() -> [Trainer]? {
        return [
            Trainer(name: "Руднев Александр", achievments: "", imageName: "tr_kar_rybak")
        ]
    }
    
    private static func getTrainersForStripDance() -> [Trainer]? {
        return [
            Trainer(name: "Диана Гончар", achievments: "", imageName: "tr_sd_diana_gonchar")
        ]
    }
    
    
    private static func getGalleryImagesForGym() -> [UIImage]? {
        return [
            UIImage(named: "gallery_gym_1")!,
            UIImage(named: "gallery_gym_2")!,
            UIImage(named: "gallery_gym_3")!,
            UIImage(named: "gallery_gym_4")!,
            UIImage(named: "gallery_gym_5")!,
            UIImage(named: "gallery_gym_6")!
        ]
    }
    
    private static func getGalleryImagesForAerobics() -> [UIImage]? {
        return [
            UIImage(named: "gallery_aerob_1")!,
            UIImage(named: "gallery_aerob_2")!,
            UIImage(named: "gallery_aerob_3")!
        ]
    }
    
    private static func getGalleryImagesForShaping() -> [UIImage]? {
        return nil
    }
    
    private static func getGalleryImagesForPilates() -> [UIImage]? {
        return nil
    }
    
    private static func getGalleryImagesForYoga() -> [UIImage]? {
        return nil
    }
    
    private static func getGalleryImagesForKickboxing() -> [UIImage]? {
        return [
            UIImage(named: "gallery_kick_1")!,
            UIImage(named: "gallery_kick_2")!,
            UIImage(named: "gallery_kick_3")!,
            UIImage(named: "gallery_kick_4")!,
            UIImage(named: "gallery_kick_5")!,
            UIImage(named: "gallery_kick_6")!
        ]
    }
    
    private static func getGalleryImagesForCapoeira() -> [UIImage]? {
        return [
            UIImage(named: "gallery_kap_1")!,
            UIImage(named: "gallery_kap_2")!,
            UIImage(named: "gallery_kap_3")!,
            UIImage(named: "gallery_kap_4")!,
            UIImage(named: "gallery_kap_5")!,
            UIImage(named: "gallery_kap_6")!
        ]
    }
    
    private static func getGalleryImagesForKarate() -> [UIImage]? {
        return nil
    }
    
    private static func getGalleryImagesForStripDance() -> [UIImage]? {
        return [
            UIImage(named: "gallery_sd_1")!,
            UIImage(named: "gallery_sd_2")!,
            UIImage(named: "gallery_sd_3")!,
            UIImage(named: "gallery_sd_4")!,
            UIImage(named: "gallery_sd_5")!
        ]
    }
    
    private static func getGalleryImagesForMassage() -> [UIImage]? {
        return nil
    }
    
    private static func getGalleryImagesForSolarium() -> [UIImage]? {
        return nil
    }
    
    private static func getGalleryImagesForCosmetology() -> [UIImage]? {
        return nil
    }

    
    
}
