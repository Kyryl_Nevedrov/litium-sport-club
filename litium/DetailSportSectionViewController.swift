//
//  DetailSportSectionViewController.swift
//  litium
//
//  Created by Kyryl Nevedrov on 10/11/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class DetailSportSectionViewController: UIViewController {
    var sportSection: SportSection!
    
    @IBOutlet weak var titleNavigationItem: UINavigationItem!
    @IBOutlet weak var sportSectionDescrioptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sportSectionDescrioptionLabel.text = sportSection.description
        titleNavigationItem.title = sportSection.title
    }
}
