//
//  EditProfileViewController.swift
//  litium
//
//  Created by Kyryl Nevedrov on 10/18/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import CloudKit

class EditProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate	 {
    @IBOutlet weak var profileEmailTextField: UITextField!
    @IBOutlet weak var profilePhoneTextField: UITextField!
    @IBOutlet weak var profileNameTextField: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    
    var profileRecord: CKRecord?
    var publicDB: CKDatabase!
    var imageChange: Bool = false
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let profileRecord = profileRecord {
            if let image = profileRecord["photo"] as? CKAsset {
                let imageData = NSData(contentsOfFile: image.fileURL.path) as! Data
                
                DispatchQueue.main.async {
                    self.profileImage.image = UIImage(data: imageData)
                }
            }
            
            DispatchQueue.main.async {
                self.profileNameTextField.placeholder = profileRecord["name"] as? String
                self.profileEmailTextField.placeholder = profileRecord["email"] as? String
                self.profilePhoneTextField.placeholder = profileRecord["telephone"] as? String
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            DispatchQueue.main.async {
                self.profileImage.image = image
                self.imageChange = true
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func activityIndicatorStart() {
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = .gray
        view.addSubview(activityIndicator)
        view.bringSubview(toFront: activityIndicator)
        activityIndicator.startAnimating()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapSelectImageButton(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func doneDidTapButton(_ sender: AnyObject) {
        activityIndicatorStart()
        profileRecord?["name"] = profileNameTextField.text as CKRecordValue?
        profileRecord?["email"] = profileEmailTextField.text as CKRecordValue?
        profileRecord?["telephone"] = profilePhoneTextField.text as CKRecordValue?
        if imageChange == true {
            let documentsURL = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
            let fileURL = documentsURL.appending("/photo.jpg")
            let image = profileImage!.image
            do {
                try UIImageJPEGRepresentation(image!, 0.5)?.write(to: URL(fileURLWithPath: fileURL))
                let imageToDownload = CKAsset(fileURL: URL(fileURLWithPath: fileURL))
                profileRecord?["photo"] = imageToDownload
                
            } catch let error{
                print(error.localizedDescription)
            }
        }
     
        publicDB.save(profileRecord!) { (record, error) in
            guard error == nil else {
                print(error?.localizedDescription)
                return
            }
            
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "unwindToProfile", sender: nil)
            }
        }
    }

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "unwindToProfile" {
            let destinationCotroller = segue.destination as! ProfileViewController
            destinationCotroller.profileRecord = profileRecord
        }
    }
    

}
