//
//  ProfileViewController.swift
//  litium
//
//  Created by Kyryl Nevedrov on 10/11/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import CloudKit

class ProfileViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profilePhoneLabel: UILabel!
    @IBOutlet weak var profileEmailLabel: UILabel!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    private var ckContainer: CKContainer!
    private var publicDB: CKDatabase!
    private var userID: CKRecordID!
    var profileRecord: CKRecord?
    var profilePaymentRecords: [CKRecord]?
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if profileRecord == nil {
            DispatchQueue.main.async {
                self.editButton.isEnabled = false
            }
            activityIndicatorStart()
        } else {
            DispatchQueue.main.async {
                self.editButton.isEnabled = true
            }
        }
        
        ckContainer = CKContainer.default()
        publicDB = ckContainer.publicCloudDatabase
        
        ckContainer.accountStatus { (accountStatus, error) in
            if( accountStatus == CKAccountStatus.noAccount) {
                let alert = UIAlertController(title: "Войдите в iCloud", message: "Для входа в личный кабинет, войдите в свой аккаунт iCloud. На главном экране запустите Settings, нажмите iCloud, и введите свой Apple ID. Включите iCloud Drive. Если у вас нет аккаунта iCloud, нажмите Create a new Apple ID.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        ckContainer.fetchUserRecordID(completionHandler: { (recordID, error) in
            guard error == nil else {
                let alert = UIAlertController(title: error!.localizedDescription, message: "", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            self.userID = recordID
            let predicate = NSPredicate(format: "user = %@", recordID!)
            let query = CKQuery(recordType: "Profile", predicate: predicate)
            
            self.publicDB.perform(query, inZoneWith: nil, completionHandler: { (records, error) in
                guard error == nil else {
                    print(error?.localizedDescription)
                    return
                    
                }
                if let record = records?.first {
                    self.profileRecord = record
                    self.updateUI(record: record)
                    let predicateForPayments = NSPredicate(format: "profile = %@", record.recordID)
                    let queryForPayments = CKQuery(recordType: "Payment", predicate: predicateForPayments)
                    
                    self.publicDB.perform(queryForPayments, inZoneWith: nil, completionHandler: { (paymentRecords, error) in
                        guard error == nil else{
                            print(error?.localizedDescription)
                            return
                        }
                        self.profilePaymentRecords = paymentRecords
                        DispatchQueue.main.async {
                            
                           self.tableView.reloadData()
                        }
                    })
                } else {
                    let profileID = CKRecordID(recordName: "profile\(recordID!.recordName)")
                    self.profileRecord = CKRecord(recordType: "Profile", recordID: profileID)
                    let userRef = CKReference(recordID: recordID!, action: .none)
                    self.profileRecord?["user"] = userRef
                    
                    self.publicDB.save(self.profileRecord!, completionHandler: { (record, error) in
                        guard error == nil else {
                            print(error?.localizedDescription)
                            return
                        }
                    })
                    self.performSegue(withIdentifier: "toEditUser", sender: nil)
                    //go to edit controller
                }
                
            })
            
        })
    }
    
    func updateUI (record: CKRecord) {
        if let image = record["photo"] as? CKAsset {
            let imageData = NSData(contentsOfFile: image.fileURL.path) as! Data
            DispatchQueue.main.async {
                self.profileImage.image = UIImage(data: imageData)
            }
        }
        
        DispatchQueue.main.async {
            self.editButton.isEnabled = true
            self.profileNameLabel.text = "Имя: \(record["name"] as! String)"
            self.profileEmailLabel.text = "Email: \(record["email"] as! String)"
            self.profilePhoneLabel.text = "Телефон: \(record["telephone"] as! String)"
            self.activityIndicator.stopAnimating()
        }
    }
    
    @IBAction func unwindToProfile(segue: UIStoryboardSegue){
        updateUI(record: profileRecord!)
    }
    
    @IBAction func updatePageDidButtonTapped(_ sender: UIBarButtonItem) {
        self.viewDidLoad()
    }
    
    func activityIndicatorStart() {
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = .gray
        DispatchQueue.main.async {
            self.view.addSubview(self.activityIndicator)
            self.view.bringSubview(toFront: self.activityIndicator)
            self.activityIndicator.startAnimating()
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toEditUser" {
            let destinationCotroller = segue.destination as! EditProfileViewController
            destinationCotroller.profileRecord = profileRecord
            destinationCotroller.publicDB = publicDB
            
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let profilePaymentRecords = profilePaymentRecords {
            return profilePaymentRecords.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = "\(profilePaymentRecords![indexPath.row]["title"]!)-\(profilePaymentRecords![indexPath.row]["amount"]!)"
        cell.detailTextLabel?.text = "\(profilePaymentRecords![indexPath.row]["end_date"]!)"
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Оплаченные занятия"
    }
}
