//
//  ContactInformationViewController.swift
//  litium
//
//  Created by Kyryl Nevedrov on 10/11/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import MapKit

class ContactInformationViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    
    let pointAnnotation = MKPointAnnotation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pointAnnotation.title = "Litium"
        pointAnnotation.subtitle = "Sport Club"
        pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: 49.4359865, longitude: 27.0142227)
        self.mapView.addAnnotation(pointAnnotation)
 
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let region = MKCoordinateRegionMakeWithDistance(pointAnnotation.coordinate, 600, 600)
        self.mapView.setRegion(region, animated: true)
    }
}
