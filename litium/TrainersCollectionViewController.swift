//
//  TrainersCollectionViewController.swift
//  litium
//
//  Created by Kyryl Nevedrov on 10/11/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit

private let reuseIdentifier = "trainersCell"

class TrainersCollectionViewController: UICollectionViewController {
    fileprivate let itemsPerRow: CGFloat = 3.0
    fileprivate let sectionInsets = UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
    
    private let sportSections = SportSection.getSportSections()
    private var sportSectionsWithTrainers: [SportSection] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sportSectionsWithTrainers = sportSections.filter {sportSection in
            return sportSection.trainers != nil
        }
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailTrainer" {
            let destinationCotroller = segue.destination as! DetailTrainersCollectionViewController
            let indexPath = collectionView!.indexPathsForSelectedItems
            destinationCotroller.trainer = sportSectionsWithTrainers[indexPath![0].section].trainers![indexPath![0].row]
        }
    }

    // MARK: UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerView", for: indexPath) as! TrainersHeadersCollectionReusableView
            headerView.label.text = sportSectionsWithTrainers[indexPath.section].title
            return headerView
        default:
            assert(false, "Unexpected element kind")
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sportSectionsWithTrainers.count
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sportSectionsWithTrainers[section].trainers!.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! TrainersCollectionViewCell
        cell.trainerNameLabel.text = sportSectionsWithTrainers[indexPath.section].trainers![indexPath.row].name
        cell.imageView.image = sportSectionsWithTrainers[indexPath.section].trainers![indexPath.row].image
        
        return cell
    }
}

extension TrainersCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - padding
        let itemWidth = availableWidth / itemsPerRow
        
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}
