//
//  TrainersHeadersCollectionReusableView.swift
//  litium
//
//  Created by Kyryl Nevedrov on 10/13/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class TrainersHeadersCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var label: UILabel!
}
