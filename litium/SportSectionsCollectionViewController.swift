//
//  SportSectionsCollectionViewController.swift
//  litium
//
//  Created by Kyryl Nevedrov on 10/11/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit

private let reuseIdentifier = "SportSectionsCell"

class SportSectionsCollectionViewController: UICollectionViewController {
    fileprivate let sectionInsets = UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
    fileprivate let itemsPerRow: CGFloat = 3.0
    
    private let sportSections: [SportSection] = SportSection.getSportSections()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
  
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailSportSection" {
            let destinationCotroller = segue.destination as! DetailSportSectionViewController
            let indexPath = collectionView!.indexPathsForSelectedItems
            destinationCotroller.sportSection = sportSections[indexPath![0].row]
        }
    }
 

    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sportSections.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! SportSectionsCollectionViewCell
        cell.imageView.image = sportSections[indexPath.row].image
        cell.sportSectionTitleLabel.text = sportSections[indexPath.row].title

        return cell
    }

}

extension SportSectionsCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let itemWidth = availableWidth / itemsPerRow
        
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
