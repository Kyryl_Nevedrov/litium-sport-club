//
//  DetailScheduleAndPricesViewController.swift
//  litium
//
//  Created by Kyryl Nevedrov on 10/11/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class DetailScheduleAndPricesViewController: UIViewController {
    @IBOutlet weak var pricesLabel: UILabel!
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var titleNavigationItem: UINavigationItem!
    
    var sportSection: SportSection!

    override func viewDidLoad() {
        super.viewDidLoad()
        pricesLabel.text = sportSection.prices
        scheduleLabel.text = sportSection.schedule
        titleNavigationItem.title = sportSection.title
    }
}
