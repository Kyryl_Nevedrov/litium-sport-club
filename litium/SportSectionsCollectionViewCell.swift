//
//  SportSectionsCollectionViewCell.swift
//  litium
//
//  Created by Kyryl Nevedrov on 10/12/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class SportSectionsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var sportSectionTitleLabel: UILabel!
}
