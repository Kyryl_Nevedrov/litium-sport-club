//
//  DetailGaleryViewController.swift
//  litium
//
//  Created by Kyryl Nevedrov on 10/11/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class DetailGaleryViewController: UIViewController {
    var image: UIImage!
    @IBOutlet weak var galleryImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        galleryImage.image = image
    }
}
