//
//  DetailTrainersCollectionViewController.swift
//  litium
//
//  Created by Kyryl Nevedrov on 10/11/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class DetailTrainersCollectionViewController: UIViewController {
    @IBOutlet weak var trainerAchievmentsLabel: UILabel!
    @IBOutlet weak var tarinerImageView: UIImageView!
    @IBOutlet weak var titleNavigationItem: UINavigationItem!
    
    var trainer: Trainer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleNavigationItem.title = trainer.name
        trainerAchievmentsLabel.text = trainer.achievments
        tarinerImageView.image = trainer.image
    }
}
