//
//  Trainer.swift
//  litium
//
//  Created by Kyryl Nevedrov on 10/11/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class Trainer{
    var name: String
    var achievments: String
    var image: UIImage?
    
    init(name: String, achievments: String, imageName: String){
        self.name = name
        self.achievments = achievments
        self.image = UIImage(named: imageName)
    }
}
