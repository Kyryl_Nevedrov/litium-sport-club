//
//  ScheduleAndPricesTableViewController.swift
//  litium
//
//  Created by Kyryl Nevedrov on 10/11/16.
//  Copyright © 2016 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class ScheduleAndPricesTableViewController: UITableViewController {
    private let sportSections = SportSection.getSportSections()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
   
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sportSections.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "scheduleAndPricesCell", for: indexPath)
        cell.textLabel?.text = sportSections[indexPath.row].title

        return cell
    }
 
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailScheduleAndPrices" {
            let destinationController = segue.destination as! DetailScheduleAndPricesViewController
            let indexPath = tableView.indexPathForSelectedRow!
            destinationController.sportSection = sportSections[indexPath.row]
        }
    }

}
